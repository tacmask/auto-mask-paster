# auto-mask-paster

Automatically paste a mask on people's face

### Requirements
- Python3.6+
- virtualenv (`pip install virtualenv`)
- Download shape_predictor_68_face_landmarks.dat and mask_edited images from https://drive.google.com/drive/folders/1QE-0szA2qpmM7X4DmBLrXixsAEVGowwZ?usp=sharing
- Add mask_edited images on images/

### Installation
- `virtualenv env`
- `source env/bin/activate` (Linux)
- `env\\Scripts\\activate.bat` (Windows)
- `pip install -r requirements.txt`

### Execution
- `python mask_paster.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg`