# USAGE
# python mask_paster.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg

# import the necessary packages
import random
from collections import OrderedDict

from imutils import face_utils
import numpy as np
import argparse
import imutils
import dlib
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--shape-predictor", required=True,
                help="path to facial landmark predictor")
ap.add_argument("-i", "--image", required=True,
                help="path to input image")
args = vars(ap.parse_args())

# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(args["shape_predictor"])

# load the input image, resize it, and convert it to grayscale
image = cv2.imread(args["image"])
image = imutils.resize(image, width=500)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# detect faces in the grayscale image
rects = detector(gray, 1)

# loop over the face detections
for (i, rect) in enumerate(rects):
    try:
        # determine the facial landmarks for the face region, then
        # convert the landmark (x, y)-coordinates to a NumPy array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        (x, y, w, h) = face_utils.rect_to_bb(rect)
        # cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # nose tip
        (i, j) = face_utils.FACIAL_LANDMARKS_IDXS['nose']
        nose_down = j - 6
        (x_nose_tip, y_nose_tip) = shape[nose_down]
        # cv2.circle(image, (x_nose_tip, y_nose_tip), 1, (0, 0, 255), -1)

        # jaw
        (i, j) = face_utils.FACIAL_LANDMARKS_IDXS['jaw']
        # for (x, y) in shape[i:j]:
        #     cv2.circle(image, (x, y), 1, (0, 0, 255), -1)
        (x_jaw, y_jaw, w_jaw, h_jaw) = cv2.boundingRect(np.array([shape[i:j]]))
        
        #paste a random mask
        n = random.randint(0, 6)
        s_img = cv2.imread(f"./images/mask_edited{n}.png", -1)
        
        x_offset = x_jaw
        y_offset = y_nose_tip
        
        width = w_jaw
        ratio = width / s_img.shape[1]
        
        s_img = cv2.resize(s_img, (int(s_img.shape[1] * ratio), int(s_img.shape[0] * ratio)))
        
        y1, y2 = y_offset, y_offset + s_img.shape[0]
        x1, x2 = x_offset, x_offset + s_img.shape[1]
        
        alpha_s = s_img[:, :, 3] / 255.0
        alpha_l = 1.0 - alpha_s
        
        for c in range(0, 3):
            image[y1:y2, x1:x2, c] = alpha_s * s_img[:, :, c] + alpha_l * image[y1:y2, x1:x2, c]
    except:
        pass        

# show the output image with the face detections + facial landmarks
cv2.imshow("Output", image)
cv2.imwrite(args["image"][:-4] + '_with_masks.jpg', image)
cv2.waitKey(0)
